import React, {Component} from 'react';
import {View, YellowBox, AsyncStorage, Text, StatusBar, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

//Screens
import FirstStart from './components/FirstStart';
import MainPage from './components/MainTab/MainPage';
import ProfileBarPage from './components/MainTab/ProfileBarPage';
import BarMap from './components/MainTab/BarMap';
import JackChat from './components/JackChat';
import FavoritePage from './components/FavoriteTab/FavoritePage';
import SharePage from './components/ShareTab/SharePage';
import SettingsPage from './components/SettingsTab/SettingsPage';
import Authorization from './components/Authorization';
import Registration from './components/Registration';
import Profile from './components/Profile/ProfileFile';
import Code from './components/Code';

//Reducer
import Reducer from './reducers';


//redux
import {createStore,} from 'redux';
import {Provider} from 'react-redux';
import {Router, Scene, Actions} from 'react-native-router-flux';

export const store = createStore(Reducer);

import {styles, colorTheme} from './components/config/config'

const TabIcon = ({focused, title, iconName}) => {
    const color = focused ? '#fff' : colorTheme.buttonsColor;
    return (
        <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center'
        }}>
            <Icon style={{color: color}} name={iconName} size={30}/>
        </View>
    );
};

const BackIcon = () => {
    return (
        <TouchableOpacity onPress={() => Actions.pop()} style={{marginLeft: 20}}>
            <View style={[styles.centerBlock, styles.circleButton]}>
                <Icon style={{color: "#fff"}} name="md-arrow-round-back" size={30}/>
            </View>
        </TouchableOpacity>
    );
};

class App extends Component {
    constructor() {
        super();
        this.state = {
            auth: false,
            loading: false
        }
        YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    }

    async componentDidMount() {
        try {
            let verify = await AsyncStorage.getItem('access_token');
            if (verify !== null) {
                return this.setState({auth: true, loading: true})
            }
            this.setState({loading: true})
        } catch (e) {
            this.setState({loading: true})
        }

    }

    render() {
        return (
            <View style={{flex: 1}}>
                {this.state.loading &&
                <View style={{flex: 1}}>
                    <StatusBar
                        backgroundColor="#000824"
                        barStyle="light-content"
                    />
                    <Provider store={store}>
                        <Router>
                            <Scene key="root" hideNavBar>
                                <Scene initial={!this.state.auth} key='FirstStart' type="reset" hideNavBar={true}
                                       component={FirstStart}/>
                                <Scene key='Authorization' type="reset" hideNavBar={true} component={Authorization}/>
                                <Scene key='Registration' hideNavBar={true} component={Registration}/>
                                <Scene key='JackChat' hideNavBar={true} component={JackChat}/>
                                <Scene
                                    initial={this.state.auth}
                                    lazy={true} key="Main" tabs={true} hideNavBar={true} tabBarPosition={"bottom"}
                                    tabBarStyle={{backgroundColor: colorTheme.tabBarBackgroundColor}}
                                    type="reset" showLabel={false} swipeEnabled={false}
                                    animationEnabled
                                >
                                    <Scene key="MainTab" iconName="md-menu" icon={TabIcon}>
                                        <Scene key='MainPage' hideNavBar={true} component={MainPage}/>
                                        <Scene key='ProfileBarPage'
                                               navigationBarStyle={{backgroundColor: colorTheme.backgroundColor}}
                                               navBarButtonColor="#FFF" hideNavBar={false} component={ProfileBarPage}/>
                                        <Scene key='BarMap' hideNavBar={true} component={BarMap}/>
                                    </Scene>
                                    <Scene key="FavoriteTab" iconName="md-heart" icon={TabIcon}>
                                        <Scene key='FavoritePage' hideNavBar={true} component={FavoritePage}/>
                                    </Scene>
                                    <Scene key="ShareTab" iconName="md-pricetag" icon={TabIcon}>
                                        <Scene key='SharePage' hideNavBar={true} component={SharePage}/>
                                    </Scene>
                                    <Scene key="SettingsTab" iconName="md-options" icon={TabIcon}>
                                        <Scene key='SettingsPage' hideNavBar={true} component={SettingsPage}/>
                                    </Scene>
                                </Scene>
                                <Scene key='Profile' hideNavBar={false}
                                       navigationBarStyle={{backgroundColor: colorTheme.backgroundColor}}
                                       navBarButtonColor="#FFF" component={Profile}/>
                                <Scene key='QRCode' hideNavBar={false}
                                       navigationBarStyle={{backgroundColor: "#171740"}}
                                       renderLeftButton={BackIcon}
                                       titleStyle={{color: "#fff", textAlign: "center", width: "80%"}}
                                       title="QR сканнер" component={Code}/>
                            </Scene>
                        </Router>
                    </Provider>
                </View>}
            </View>
        );
    }
}

export default App;