import Api from "./api";

export async function getFile(id) {
    let res = await Api.get("api/UploadedFiles/"+id,null ,false, true);
    let data = await res.json()
    // if (res.status !== 200) throw new Error(data.error_description)
    return data
}