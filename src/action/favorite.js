import Api from "./api";

export async function AddFavorite(id,set) {
    let res = await Api.get('api/Agents/'+id+`?setAsFavorite=`+set);
    return res
}

export async function getFavorite(page) {
    let res = await Api.get(`api/Agents?pageLength=10&pageNumber=`+page+`&inFavorites=true`,true);
    let data = await res.json()
    console.log(data)
    // if (res.status !== 200) throw new Error(data.error_description)
    return data
}