import Api from "./api";

export async function GetOffers(id) {
    let res = await Api.get(`api/Offers?isActive=true&inFavorites=true`, true);
    let data = res.json()
    return data
}

export async function GetOffersAgent(id) {
    let res = await Api.get(`api/Offers?AgentId=`+id+'&isActive=true', true);
    let data = res.json()
    return data
}
