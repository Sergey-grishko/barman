// import { store } from '../index'

import {AsyncStorage} from "react-native";

export default class Api{

    static host = "http://dev.bidon-tech.com/BarmanJack/";

    static async getTokenFromLocalStorage(){
        return await AsyncStorage.getItem('access_token');
    }

    static async setTokenToLocalStorage(token){
        return await AsyncStorage.setItem('access_token', token);
    }

    static deleteTokenFromLocalStorage(){
        return AsyncStorage.removeItem('access_token');
    }

    static async get(endpoint, authorization = true){
        let headers = {

        }

        if(authorization) headers.Authorization = 'Bearer ' + await Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'GET',
            headers
        })
    }

    static post(endpoint, body = null, authorization = true, headers = {}, convertToJSON = true){

        if (convertToJSON) body = JSON.stringify(body)
        if(headers !== false && Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }
        if (headers === false) headers = {}
        console.log(body)
        if(authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()
        return fetch(`${Api.host}${endpoint}`, {
            mode: 'no-cors',
            method: 'POST',
            headers,
            body
        })
    }

    static put(endpoint, body = null, authorization = true, headers = {}){
        body = JSON.stringify(body)

        if(Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }

        if(authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'PUT',
            headers,
            body
        })
    }

    static deletion(endpoint, body = null, authorization = true, headers = {}) {
        body = JSON.stringify(body)

        if (Object.keys(headers).length === 0) headers = {
            'Content-Type': 'application/json',
            'Content-Length': body ? body.length : 0
        }

        if (authorization) headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()

        return fetch(`${Api.host}${endpoint}`, {
            mode: 'cors',
            method: 'DELETE',
            headers,
            body
        })
    }

    // static getLanguage(){
    //     let language = localStorage.getItem('language');
    //     if(!language) language = navigator.language || navigator.userLanguage
    //     if(!language || ['en','nl'].indexOf(language) < 0) language = 'en'
    //
    //     return language
    // }
}