import Api from './api'
import qs from 'qs'
import {AsyncStorage} from 'react-native'


export async function signup(body) {
    let res = await Api.post("api/Account/Register", body, false, true);
    let data = await res.json()
    console.log(res)
    console.log(data)
    console.log(data.Message)
    if (res.status !== 201) throw new Error(data.Message)
    return data
}

export async function signin(body) {
    let headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
    let res = await Api.post("token",qs.stringify(body),false, headers, false);
    let data = await res.json()
    console.log(res)
    console.log(data)
    AsyncStorage.setItem("access_token", data.access_token)
    if (res.status !== 200) throw new Error(data.error_description)
    return data
}

