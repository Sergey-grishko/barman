import React, { Component } from 'react';
import {
    View, Text,TouchableNativeFeedback,
    TextInput,FlatList, Animated,
    Dimensions,TouchableWithoutFeedback,ScrollView,
    PanResponder, Switch
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {CachedImage} from 'react-native-cached-image';

import {styles,colorTheme,arrayShapePage} from '../config/config'

import Curtain from '../Curtain/_Curtain'
import CurtainView from '../Curtain/_CurtainView'
import TopBarView from '../Curtain/_TopBarView'


class SettingsPage extends Component{
    static navigationOptions = ({ navigation }) => {
        return {
            tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
                const { route, index, focused} = scene;
                if(focused){
                    if(navigation.state.params.menuStatus()) navigation.state.params.hideMenu()
                }
                jumpToIndex(3)
            }
        }
    };
    constructor(props){
        super(props);
        this.state={
            curtainOpen: false,
            pushNotification:false,
            autoResetGeo:true,
            _onChangeGEO:''
        };
        this.onChangeSwitch = this.onChangeSwitch.bind(this)
    }
    onChangeSwitch(field){
        this.setState({[field]:!this.state[field]})
    }
    componentDidMount() {
        this.props.navigation.setParams({
            hideMenu: this.hideMenu.bind(this),
            menuStatus: this.menuStatus.bind(this)
        })
    }

    menuStatus() {
        return this.state.curtainOpen
    };

    hideMenu=()=>{
        this.setState({curtainOpen:false})
    };

    render() {
        return (
            <Curtain
                visible={this.state.curtainOpen}
                onSwipeCurtain={()=> this.setState({curtainOpen:false})}
                curtainViewComponent={<CurtainView curtainClose={()=>this.setState({curtainOpen:false})}/>}
                topBarViewComponent={
                    <TopBarView
                        curtainOpen={this.state.curtainOpen}
                        onChange={()=>this.setState({curtainOpen:!this.state.curtainOpen})}
                    />
                }
            >
                <View style={styles.mainBlock}>
                    <ScrollView>
                        <View style={{paddingVertical:10,paddingHorizontal:20,backgroundColor:colorTheme.tabBarBackgroundColor}}>
                            <View style={styles.switchBlock}>
                                <Text style={styles.textInSwitchBlock}>Push-уведомления</Text>

                            </View>
                            <View style={styles.switchBlock}>
                                <Text style={styles.textInSwitchBlock}>Автоматическое обновление местоположения</Text>

                            </View>
                            <Text style={{color:'white'}}>Изменение региона</Text>
                            <View>
                                <TextInput
                                    style={{color:'white'}}
                                    placeholder="Выбор региона"
                                    placeholderTextColor="#fff"
                                    underlineColorAndroid={"#fff"}
                                    onChangeText={(_onChangeGEO) => this.setState({_onChangeGEO})}
                                    value={this.state._onChangeGEO}
                                />
                                <MaterialCommunityIcons style={{
                                    color: '#fff',position:'absolute',right:10,bottom:20
                                }} name={'arrow-down-bold'} size={15}/>
                            </View>
                        </View>
                        <View style={{paddingVertical:20,paddingHorizontal:20}}>
                            <Text style={{color:'white'}}>Авторизация</Text>
                            <View style={[styles.authButtons,{backgroundColor:'#00629d'}]}>
                                <Text style={{color:'white'}}>Через FACEBOOK</Text>
                                <Icon style={{color: '#fff'}} name={'logo-facebook'} size={30}/>
                            </View>
                            <View style={[styles.authButtons,{backgroundColor:'#f34733'}]}>
                                <Text style={{color:'white'}}>Через GOOGLE</Text>
                                <Icon style={{color: '#fff'}} name={'logo-googleplus'} size={30}/>
                            </View>
                        </View>
                        <View style={{flex: 1,alignItems:'center',flexDirection:'row', paddingHorizontal:20 }}>
                            <View style={{flex: 1,paddingVertical:20,paddingRight:10}}>
                                <View style={[styles.centerBlock,styles.circleButton]}>
                                    <Icon style={{color: '#fff'}} name={'md-information'} size={25}/>
                                </View>
                            </View>
                            <View style={{flex: 5,paddingHorizontal:10,borderLeftWidth:1,borderColor:'white'}}>
                                <Text style={{ flexWrap: 'wrap', color:'white'}}>Авторизация необходима для закрепления за вами списка избранных заведений, скидок и спец предложений. Пока вы не авторизированы все перечисленные пункты закреплены за IMEI вашего устройства.</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </Curtain>
        );
    }
}

export default SettingsPage;
