import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableNativeFeedback,
    TextInput,
    FlatList,
    Animated,
    Dimensions,
    TouchableWithoutFeedback,
    ScrollView,
    AsyncStorage,
    PanResponder
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {CachedImage} from 'react-native-cached-image';

import {styles, colorTheme, arrayShapePage} from '../config/config'

import Curtain from '../Curtain/_Curtain'
import CurtainView from '../Curtain/_CurtainView'
import TopBarView from '../Curtain/_TopBarView'
import * as getFavorite from '../../action/favorite'
import * as Offers from '../../action/offers'
import BallIndicator from "react-native-indicators/src/components/ball-indicator";

var {height, width} = Dimensions.get('window');

class SharePage extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
                const {route, index, focused} = scene;
                if (focused) {
                    if (navigation.state.params.menuStatus()) navigation.state.params.hideMenu()
                }
                jumpToIndex(2)
            }
        }
    };

    constructor() {
        super();
        this.state = {
            curtainOpen: false,
            openShapesMenu: [],
            Bars: null,
            Offers: [],
            refreshing:false
        }
    }

    async componentDidMount() {
        await this.onUploadData()
        this.props.navigation.setParams({
            scrollToTop: this.callScrollToTop.bind(this),
            hideMenu: this.hideMenu.bind(this),
            menuStatus: this.menuStatus.bind(this)
        })
    }

    menuStatus() {
        return this.state.curtainOpen
    }

    async onUploadData() {
        this.setState({refreshing: true});
        let bar = await getFavorite.getFavorite(1, '')
        console.log(bar)
        let offer = await Offers.GetOffers()
        console.log(offer)
        this.setState({
            openShapesMenu: offer.Records,
            Bars: bar.Records,
            Offers: offer.Records,
            refreshing:false
        })
    }

    hideMenu = () => {
        this.setState({curtainOpen: false})
    }

    callScrollToTop = () => {
        this.flatList.scrollToOffset({offset: 0, animated: true});
    }

    hidePartMenu(ref, i) {
        let arrayBars = this.state.Offers;
        arrayBars[i].open = false;
        this.setState({
            Offers: arrayBars
        }, () => {
            this[ref].setValue(1);
            Animated.timing(
                this[ref],
                {toValue: 0, friction: 4}
            ).start();
        });
    }

    openPartMenu(ref, i) {
        if (this.state.openShapesMenu[i].open) {
            return this.hidePartMenu(ref, i)
        }
        let arrayBars = this.state.openShapesMenu;
        arrayBars[i].open = true;
        this.setState({openShapesMenu: arrayBars}, () => {
            this[ref].setValue(0);
            Animated.timing(this[ref], {toValue: 1}).start();
        });
    }


    flatListShapesArray(item) {
        return (
            <FlatList
                data={item}
                initialNumToRender={3}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => {
                    return (
                        <View style={{
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <Icon style={{
                                color: colorTheme.buttonsColor,
                                marginRight: 20
                            }} name={'md-pricetag'} size={30}/>
                            <View>
                                <Text style={{color: 'white', fontSize: 16}}>{item.Name}</Text>
                                <Text style={{color: 'white', fontSize: 12, width: 250}}>{item.Description}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        )
    }

    oneBarShape(item, i) {
        console.log(item)
        this[item.Id] = new Animated.Value(0);
        // if (item.OffersCnt !== 0) {
            let arr = [];
            let offer = this.state.Offers.length === 0 ? null : this.state.Offers.map(value => {
                if (item.Id === value.AgentId) {
                    arr.push(value)
                }
            })
            return (
                <View>
                    <TouchableNativeFeedback onPress={() => {
                        this.openPartMenu(item.Id, i)
                    }}>
                        <View style={styles.oneBarShapeView}>
                            <View>
                                <Text style={{color: 'white', fontSize: 12}}>Предложение от</Text>
                                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                                    <Text
                                        style={{color: 'white', fontSize: 18, fontWeight: 'bold', width:200}}>{item.Name}</Text>
                                    <Text style={{color: 'white', fontSize: 16, marginLeft: 10}}>{item.length} 0
                                        М</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <View style={[styles.centerBlock, {marginRight: 10}]}>
                                    <Icon style={{color: colorTheme.buttonsColor}} name={'md-pricetag'} size={35}/>
                                    <Text style={{position: 'absolute', color: 'white'}}>{item.OffersCnt}</Text>
                                </View>
                                <Icon style={{color: colorTheme.buttonsColor}} name={'md-heart'} size={30}/>
                            </View>
                            <Icon style={{color: '#fff', position: 'absolute', right: 27, bottom: 5}}
                                  name={'ios-arrow-down-outline'} size={15}/>
                        </View>
                    </TouchableNativeFeedback>
                    <Animated.View
                        style={{
                            marginTop: this[item.Id].interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, -arr.length * 60]
                            }),
                            zIndex: 50,
                        }}
                    >
                        {this.flatListShapesArray(arr)}
                    </Animated.View>
                </View>)
        // }
    }

    render() {
        return (
            <Curtain
                visible={this.state.curtainOpen}
                onSwipeCurtain={() => this.setState({curtainOpen: false})}
                curtainViewComponent={<CurtainView curtainClose={() => this.setState({curtainOpen: false})}/>}
                topBarViewComponent={
                    <TopBarView
                        curtainOpen={this.state.curtainOpen}
                        onChange={() => this.setState({curtainOpen: !this.state.curtainOpen})}
                    />
                }
            >
                <View style={styles.mainBlock}>
                    {this.state.Bars !== null ? (this.state.Bars.length === 0 ? (
                        <View style={{
                            position: "absolute",
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            flexDirection: 'row'
                        }}><Icon style={{color: colorTheme.buttonsColor, marginRight: 5}} name='ios-alert' size={35}/>
                            <Text style={{
                                color: "#fff",
                                fontSize: 20,
                            }}>Для вас не акций</Text>
                        </View>
                    ) : null) : null}
                    {this.state.Bars === null ? <BallIndicator color="#a000ad"/> : (
                        <FlatList
                            ref={(ref) => {
                                this.flatList = ref;
                            }}
                            data={this.state.Bars}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => this.oneBarShape(item, index)}
                            initialNumToRender={4}
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onUploadData()}
                        />)}
                </View>
            </Curtain>
        );
    }
}

export default SharePage;
