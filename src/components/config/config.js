import React, { Component } from 'react';
import {StyleSheet} from 'react-native';

export const colorTheme = {
    buttonsColor:'#a000ad',
    backgroundColor:'#0a1132',
    tabBarBackgroundColor:'#211c40'
};

export const styles = StyleSheet.create({
    oneBarShapeView:{
        paddingVertical:10,
        paddingHorizontal:20,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:colorTheme.tabBarBackgroundColor,
        zIndex:60,
    },
    mainBlock:{
        flex:1,
        backgroundColor:colorTheme.backgroundColor
    },
    centerBlock:{
        justifyContent:'center',
        alignItems:'center'
    },
    circleButton:{
        width:45,
        height:45,
        backgroundColor:colorTheme.buttonsColor,
        borderRadius:50,
        borderWidth:1,
        borderColor:colorTheme.buttonsColor
    },
    barListTitle:{
        color:'white',
        fontSize:18,
        fontWeight: 'bold',
        marginBottom:5
    },
    barListLocation:{
        color:'white',
        marginLeft:10,
        fontSize:11,
        marginBottom:5
    },
    barListLengthPosition:{
        color:'white',
        fontSize:16
    },
    filterTitle:{
        color:'white',
        fontSize:18,
        marginTop:20,
        marginLeft:20
    },
    filterView:{
        position:'absolute',
        zIndex:100
    },
    topMenu:{
        alignItems:'center',
        paddingHorizontal:20,
        height:60,
        flexDirection:'row',
        backgroundColor:colorTheme.backgroundColor,
        zIndex:101
    },
    switchBlock:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        marginBottom:20
    },
    textInSwitchBlock:{flex: 1, flexWrap: 'wrap', color:'white', fontSize:16},
    authButtons:{
        marginTop:10,
        width:200,
        paddingHorizontal:20,
        paddingVertical:10,
        borderRadius:25,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    outlineButton:{
        flexDirection:'row',
        paddingVertical:5,paddingHorizontal:10,justifyContent:'center',
        alignItems:'center'
    },
    filterSearchIcon:{color: '#fff',position:'absolute',right:10,bottom:20},
    filterEnd:{height:70,flexDirection:'row',justifyContent:'flex-end',alignItems:'flex-end'},
    hideFilterButton:{color:'white',marginRight:20,textDecorationLine:"underline",textDecorationColor:"white"}

})

export const shapesArray=[
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    },
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    },
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    },
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    },
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    },
    {
        type:'',
        title:'Бесплатная чашка кофе за check-in',
        subTitle:'Действует только сегодня'
    }
];


export const arrayShapePage = [
    {
        id:'firstBar',
        barName:'One smile bar',
        length:250,
        countShapes:2,
        shapesArray:[
            {
                type:'',
                title:'Бесплатная чашка кофе за check-in',
                subTitle:'Действует только сегодня'
            },
            {
                type:'',
                title:'Бесплатная чашка кофе за check-in',
                subTitle:'Действует только сегодня'
            }
        ]
    },
    {
        id:'secondBar',
        barName:'One smile bar',
        length:250,
        countShapes:2,
        shapesArray:[
            {
                type:'',
                title:'Бесплатная чашка кофе за check-in',
                subTitle:'Действует только сегодня'
            },
            {
                type:'',
                title:'Бесплатная чашка кофе за check-in',
                subTitle:'Действует только сегодня'
            },
            {
                type:'',
                title:'Бесплатная чашка кофе за check-in',
                subTitle:'Действует только сегодня'
            }
        ]
    }
]

export const bars = [
    {
        photo:require("../../images/bar1.png"),
        title:'One Smile Bar',
        location:'ул. Шевченко 220',
        lengthPosition:'250 м.',
        countShapes:2,
        favorite:false,
    },
    {
        photo:require("../../images/bar2.png"),
        title:'Camping Village',
        location:'ул. Байды Вишневецкого, 47',
        lengthPosition:'700 м.',
        countShapes:2,
        favorite:true,
    },
    {
        photo:require("../../images/bar3.png"),
        title:'One Smile Bar',
        location:'ул. Шевченко 220',
        lengthPosition:'250 м.',
        countShapes:2,
        favorite:false,
    },
    {
        photo:require("../../images/bar1.png"),
        title:'One Smile Bar',
        location:'ул. Шевченко 220',
        lengthPosition:'250 м.',
        countShapes:2,
        favorite:false,
    },
    {
        photo:require("../../images/bar1.png"),
        title:'One Smile Bar',
        location:'ул. Шевченко 220',
        lengthPosition:'250 м.',
        countShapes:2,
        favorite:false,
    }
]

