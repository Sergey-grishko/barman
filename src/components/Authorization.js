import React, {Component} from 'react';
import {View, Text, TextInput,TouchableOpacity, TouchableNativeFeedback, ToastAndroid} from 'react-native';
import {styles, colorTheme} from './config/config'
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from 'react-native-router-flux';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import GoogleSignIn from 'react-native-google-sign-in'
import * as Account from "../action/account";
import {BallIndicator} from 'react-native-indicators';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

class Authorization extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            user: null,
            error: "",
            loading:false
        }
        this.onLogin = this.onLogin.bind(this)
    }

    onLoginOrRegisterFacebook = () => {
        LoginManager.logInWithReadPermissions(['public_profile', 'email'])
            .then((result) => {
                if (result.isCancelled) {
                    return Promise.reject(new Error('The user cancelled the request'));
                }
                return AccessToken.getCurrentAccessToken();
            })
            .then((data) => {
                console.log(data)
            })
            .catch((error) => {
                ToastAndroid.show('Error', ToastAndroid.SHORT);
            });
    }


    async onLogin() {
        this.setState({
            error: '',
            loading:true
        })
        try {
            if (this.state.email === '') throw new Error("Поле E-mail пустое");
            if (this.state.password === '') throw new Error("Поле Пароль пустое");
            if (reg.test(this.state.email) === false) throw new Error("E-mail введен неверно");
            let body = {
                username: this.state.email,
                password: this.state.password,
                grant_type: "password"
            }
            let response = await Account.signin(body);
            Actions.Main()
        } catch (error) {
            this.setState({
                error: error.message,
                loading:false
            })
        }
    }


    async onLoginOrRegisterGoogle() {
        await GoogleSignIn.configure({
            clientID: '139854464643-ni15k7hk6h2fff223prissd0mt3j57qb.apps.googleusercontent.com',
            scopes: ['openid', 'email', 'profile'],
            shouldFetchBasicProfile: true,
        });
        const user = await GoogleSignIn.signInPromise();
        setTimeout(() => {
            console.log(JSON.stringify(user, null, '  '));
        }, 1500);
    }

    render() {
        return (
            <View style={styles.mainBlock}>
                <View style={this.state.loading === true ?{flex: 1, justifyContent: 'center', paddingHorizontal: 30, opacity:0.5}:{flex: 1, justifyContent: 'center', paddingHorizontal: 30}}>
                    <View style={{alignItems: 'center', marginBottom: 30}}>
                        <Text style={{color: 'white', fontSize: 18}}>Barman Jack</Text>
                    </View>
                    <TextInput
                        ref='email'
                        onSubmitEditing={()=>this.refs.password.focus()}
                        returnKeyType="next"
                        keyboardType="email-address"
                        style={{color: 'white', width: '100%'}}
                        placeholder="E-mail"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                    />
                    <TextInput
                        ref='password'
                        returnKeyType="send"
                        onSubmitEditing={() => this.onLogin()}
                        secureTextEntry={true}
                        style={{color: 'white', width: '100%'}}
                        placeholder="Пароль"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                    />
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20}}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{borderRadius: 50, marginRight: 5}}>
                                <TouchableNativeFeedback
                                    background={TouchableNativeFeedback.Ripple('#fff', true)}
                                    useForeground={false}
                                    onPress={() => this.onLoginOrRegisterFacebook()}>
                                    <View style={[styles.centerBlock, styles.circleButton, {
                                        backgroundColor: '#00629d',
                                        borderWidth: 0
                                    }]}>
                                        <Icon style={{color: '#fff'}} name={'logo-facebook'} size={25}/>
                                    </View>
                                </TouchableNativeFeedback>
                            </View>
                            <View style={{borderRadius: 50, marginRight: 5}}>
                                <TouchableNativeFeedback
                                    background={TouchableNativeFeedback.Ripple('#fff', true)}
                                    useForeground={false}
                                    onPress={() => this.onLoginOrRegisterGoogle()}>
                                    <View style={[styles.centerBlock, styles.circleButton, {
                                        backgroundColor: '#f34733',
                                        borderWidth: 0
                                    }]}>
                                        <Icon style={{color: '#fff'}} name={'logo-googleplus'} size={25}/>
                                    </View>
                                </TouchableNativeFeedback>
                            </View>
                        </View>
                        <TouchableOpacity  onPress={() => this.onLogin()}>
                        <View style={{borderRadius: 25, borderWidth: 1, borderColor: colorTheme.buttonsColor}}>
                                <View style={[styles.outlineButton, {paddingTop: 5}]}>
                                    <Text style={{color: '#fff', fontSize: 12}}>ВОЙТИ</Text>
                                    <Icon style={{color: '#fff', marginLeft: 10}} name={'ios-play-outline'} size={25}/>
                                </View>
                        </View>
                    </TouchableOpacity>
                    </View>
                    <Text style={{
                        color: 'red',
                        fontSize: 16,
                        textAlign: "center",
                        marginTop: 10
                    }}>{this.state.error}</Text>
                </View>
                <View style={this.state.loading === false?{display:'none'}:{
                    position: "absolute",
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                }}>
                    <BallIndicator color="#a000ad"/>
                </View>
                <View style={{justifyContent: 'flex-end', alignItems: 'center', marginBottom: 30}}>
                    <TouchableNativeFeedback onPress={() => Actions.Registration()}>
                        <Text style={{textDecorationLine: 'underline', color: 'white'}}>Регистрация</Text>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );
    }

}

export default Authorization;
