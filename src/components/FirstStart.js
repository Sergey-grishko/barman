import React, { Component } from 'react';
import {View, Text, TextInput,Image,TouchableNativeFeedback} from 'react-native';
import {styles,colorTheme} from './config/config'
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

const bg = require("../images/bg.png")

class FirstStart extends Component{
    constructor(){
        super();
        this.state={
            _location:''
        }
    }
    render() {
        return (
            <View style={styles.mainBlock}>
                    <Image
                        style={{
                      backgroundColor: '#000000',
                      flex: 1,
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      justifyContent: 'center',
                    }}
                        source={bg}
                    />
                    <View style={{flex:1,justifyContent:'center',paddingHorizontal:30}}>

                        <View style={{alignItems:'center',marginBottom:30}}>
                            <View
                                style={{
                                marginTop:-30,
                                marginBottom:20,
                                borderColor:colorTheme.buttonsColor,
                                borderRadius:50,borderWidth:4,width:80,
                                height:80,justifyContent:'center',alignItems:'center'
                            }}
                            >
                                <View
                                    style={{
                                    width:60,height:60,
                                    backgroundColor:colorTheme.buttonsColor,
                                    borderRadius:50
                                }}
                                />
                            </View>
                            <Text style={{color:'white',fontSize:18}}>Barman Jack</Text>
                        </View>
                        <View style={{justifyContent:'center'}}>
                            <TextInput
                                style={{color:'white'}}
                                placeholder="Местонахождение"
                                placeholderTextColor="#fff"
                                underlineColorAndroid={colorTheme.buttonsColor}
                                onChangeText={(_location) => this.setState({_location})}
                                value={this.state._location}
                            />
                            <Icon style={{color: '#fff',position:'absolute',right:10}} name={'ios-pin-outline'} size={15}/>
                        </View>
                        <View style={{alignItems:'center',marginTop:30}}>
                            <View style={{borderRadius:25}}>
                                <TouchableNativeFeedback
                                    onPress={()=>Actions.Authorization()}
                                    background={TouchableNativeFeedback.Ripple('#52005f',true)}
                                    useForeground={false}
                                >
                                    <View
                                        style={{
                                        backgroundColor:colorTheme.buttonsColor,
                                        justifyContent:'space-between',width:200,
                                        alignItems:'center',flexDirection:'row',paddingVertical:10,paddingHorizontal:20,borderRadius:25
                                    }}
                                    >
                                        <Text style={{color:'#fff',fontSize:16,fontWeight:'bold'}}>ПРОДОЛЖИТЬ</Text>
                                        <Icon style={{color: '#fff',marginLeft:10,fontWeight:'bold'}} name={'ios-play-outline'} size={25}/>
                                    </View>
                                </TouchableNativeFeedback>
                            </View>
                        </View>
                    </View>
            </View>
        );
    }

}

export default FirstStart;
