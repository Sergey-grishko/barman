import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableNativeFeedback,
    TextInput,
    FlatList,
    Animated,
    Dimensions,
    TouchableWithoutFeedback,
    ScrollView,
    PanResponder,
    AsyncStorage,
    ToastAndroid
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {CachedImage} from 'react-native-cached-image';
import _ from 'lodash';
import {styles, colorTheme, bars} from '../config/config'

import Curtain from '../Curtain/_Curtain'
import CurtainView from '../Curtain/_CurtainView'
import TopBarView from '../Curtain/_TopBarView'
import * as Favorites from "../../action/favorite";
import {BallIndicator} from 'react-native-indicators';
import {Actions} from "react-native-router-flux";

class OneBar extends Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps.item, this.props.item)
    }

    render() {
        const {item, addFavorite} = this.props;
        return (
            <TouchableWithoutFeedback
                delayLongPress={6000}
                onLongPress={() => addFavorite()}
                onPress={() => {
                    Actions.ProfileBarPage({bar: item})
                }}
            >
                <View style={{flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 20}}>
                    <CachedImage style={{width: 125, height: 125, borderRadius: 10}}
                                 source={{uri: `http://192.168.10.98/BarmanJack/api/UploadedFiles/` + item.LogoImageFileId + `/download`}}/>
                    <View style={{marginLeft: 20, flex: 1}}>
                        <Text style={[styles.barListTitle]}>{item.Name}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Icon style={{color: '#fff'}} name={'ios-pin-outline'} size={15}/>
                            <Text numberOfLines={1} style={[styles.barListLocation, {flex: 1}]}>{item.Address}</Text>
                        </View>
                        <Text style={styles.barListLengthPosition}>{item.lengthPosition}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <View style={[styles.centerBlock, {marginRight: 10}]}>
                                <Icon style={{color: colorTheme.buttonsColor}} name={'md-pricetag'} size={35}/>
                                <Text style={{position: 'absolute', color: 'white'}}>{item.OffersCnt}</Text>
                            </View>
                            <Icon onPress={() => addFavorite()}
                                  style={{color: colorTheme.buttonsColor, paddingHorizontal: 10}}
                                  name={item.InFavorites ? 'md-heart' : 'ios-heart-outline'} size={35}/>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

class FavoritePage extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
                const {route, index, focused} = scene;
                // if(focused){
                //     if(navigation.state.params.menuStatus()) navigation.state.params.hideMenu()
                //     else navigation.state.params.scrollToTop()
                // }
                jumpToIndex(1)
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            Favorite: null,
            curtainOpen: false,
            refreshing: false,
            page: 1,
            name: ''
        }
        this.onUploadData = this.onUploadData.bind(this)
    }


    async componentDidMount() {
        await this.onUploadData()
        this.props.navigation.setParams({
            scrollToTop: this.callScrollToTop.bind(this),
            hideMenu: this.hideMenu.bind(this),
            menuStatus: this.menuStatus.bind(this)
        })
    }

    async onUploadData() {
        this.setState({refreshing: true});
        let res = await Favorites.getFavorite(1, this.state.name)
        this.setState({Favorite: res.Records, refreshing: false, page: 1})
    }

    // async onEndList() {
    //     let res = await Favorite.getFavorite(this.state.page, this.state.name)
    //     let arr = this.state.Favorite
    //     let arrNew = arr.concat(res.Records)
    //     this.setState({Favorite: arrNew, page: this.state.page + 1});
    // }

    addFavorite = async (i) => {
        let Favorite = _.cloneDeep(this.state.Favorite);
        Favorite[i].InFavorites = !Favorite[i].InFavorites;
        this.setState({Favorite})
        try {
            let res = await Favorites.AddFavorite(Favorite[i].Id, Favorite[i].InFavorites)
            if (res.status !== 204) throw new Error()
        } catch (e) {
            ToastAndroid.show('Ошибка попробуйте еще раз!', ToastAndroid.SHORT);
        }

    }


    menuStatus() {
        return this.state.curtainOpen
    };

    hideMenu = () => {
        this.setState({curtainOpen: false})
    };

    callScrollToTop = () => {
        this.flatList.scrollToOffset({offset: 0, animated: true});
    };

    render() {
        return (
            <Curtain
                visible={this.state.curtainOpen}
                onSwipeCurtain={() => this.setState({curtainOpen: false})}
                curtainViewComponent={<CurtainView curtainClose={() => this.setState({curtainOpen: false})}/>}
                topBarViewComponent={
                    <TopBarView
                        curtainOpen={this.state.curtainOpen}
                        onChange={() => this.setState({curtainOpen: !this.state.curtainOpen})}
                    />
                }
            >
                <View style={styles.mainBlock}>
                    {this.state.Favorite !== null ? (this.state.Favorite.length === 0 ? (
                        <View style={{
                            position: "absolute",
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            flexDirection: 'row'
                        }}><Icon style={{color: colorTheme.buttonsColor, marginRight: 5}} name='ios-alert' size={35}/>
                            <Text style={{
                                color: "#fff",
                                fontSize: 20,
                            }}>Вы еще ничего не
                                добавили</Text>
                        </View>
                    ) : null) : null}
                    {this.state.Favorite !== null ? (
                        (<FlatList
                            ref={(ref) => {
                                this.flatList = ref;
                            }}
                            style={{zIndex: 10}}
                            data={_.cloneDeep(this.state.Favorite)}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => <OneBar item={item}
                                                                   addFavorite={() => this.addFavorite(index)}/>}
                            initialNumToRender={4}
                            refreshing={false}
                            onRefresh={() => this.onUploadData()}
                            // onEndReached={() => this.onEndList()}
                            // onEndReachedThreshold={3}
                        />)) : <BallIndicator color="#a000ad"/>}
                </View>
            </Curtain>
        );
    }

}

export default FavoritePage;
