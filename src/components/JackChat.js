import React, { Component } from 'react';
import {View, Text, TextInput,Image,TouchableNativeFeedback,ScrollView,Dimensions,Keyboard} from 'react-native';
import {styles,colorTheme} from './config/config'
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';

const messages=[
    {
        message: 'First message',
        createDate: moment(new Date()).format("HH:mm")
    },
    {
        message: 'Second message',
        createDate: moment(new Date()).format("HH:mm")
    },
    {
        message: 'Third message',
        createDate: moment(new Date()).format("HH:mm")
    },
    {
        message: 'Fourth message',
        createDate: moment(new Date()).format("HH:mm")
    },
    {
        message: 'Fifth message',
        createDate: moment(new Date()).format("HH:mm")
    }
]

class FirstStart extends Component{
    constructor(){
        super();
        this.state={
            _messageArray:messages,
            _message:''
        };
        this.newMessage=this.newMessage.bind(this)
        this._keyboardDidShow=this._keyboardDidShow.bind(this)
    }

    componentDidMount(){
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.scrollView.scrollToEnd();

    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow () {
        this.scrollView.scrollToEnd({animated: true});
    }

    _keyboardDidHide () {
    }

    newMessage(){
        if(this.state._message==='') return;
        let newArray = this.state._messageArray;
        newArray.push({
            message: this.state._message,
            createDate: moment(new Date()).format("HH:mm")
        })
        this.setState({
            _messageArray: newArray,
            _message:''
        })
    }

    render() {
        const _messageArray= this.state._messageArray.map((item,index)=>(
            <View key={index} style={{marginRight:20,marginBottom:10,alignItems:'flex-end'}}>
                <View style={{
                        borderRadius:30,
                        maxWidth:'80%',
                        paddingHorizontal:25,
                        paddingVertical:15,
                        backgroundColor:colorTheme.buttonsColor
                    }}>
                    <Text style={{color:'white'}}>
                        {item.message}
                    </Text>
                </View>
                <Text style={{color:'white', fontSize:12,marginTop:5}}>{item.createDate}</Text>
            </View>
        ))
        return (
            <View style={styles.mainBlock}>
                <View style={[styles.topMenu,{backgroundColor:colorTheme.tabBarBackgroundColor,justifyContent:'space-between'}]}>
                    <TouchableNativeFeedback onPress={()=>Actions.pop()}>
                        <View style={[styles.centerBlock,styles.circleButton]}>
                            <Icon style={{color: '#fff'}} name={'md-arrow-back'} size={25}/>
                        </View>
                    </TouchableNativeFeedback>
                    <View style={{flexDirection:'row', alignItems:'center',maxWidth:200}}>
                        <Image
                            style={{width:45,height:45,borderRadius:50,borderWidth:1}}
                            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
                        />
                        <View style={{marginLeft:10}}>
                            <Text style={{color:'white',fontSize:16,fontWeight:'bold'}}>Camping Village</Text>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Icon style={{color: '#fff'}} name={'ios-pin-outline'} size={15}/>
                                <Text
                                    numberOfLines={1}
                                    style={{color:'white', marginLeft:5,maxWidth:150,fontSize:12}}
                                >ул. Байды Вишневецкого, 47</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView
                    contentContainerStyle={{paddingBottom:60}}
                    ref={ref => this.scrollView = ref}
                    onContentSizeChange={(contentWidth, contentHeight)=>{
                        this.scrollView.scrollToEnd({animated: true});
                    }}
                >
                    <View style={{marginLeft:20,marginBottom:10}}>
                        <View style={{
                            borderRadius:30,
                            maxWidth:'80%',
                            paddingHorizontal:25,
                            paddingVertical:15,
                            backgroundColor:colorTheme.tabBarBackgroundColor
                        }}>
                            <Text style={{color:'white'}}>
                                Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,
                                Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,
                                Lorem ipsun,Lorem ipsun,Lorem ipsun,Lorem ipsun,
                            </Text>
                        </View>
                        <Text style={{color:'white', fontSize:12,marginTop:5}}>17:55</Text>
                    </View>
                    {_messageArray}
                </ScrollView>
                <View style={{position:'absolute', left: 0, right: 0, bottom:0, backgroundColor:colorTheme.backgroundColor}}>
                    <View style={{
                        marginHorizontal:15,
                        marginVertical:10,
                        height:50,
                        borderRadius:35,
                        borderWidth:1,
                        backgroundColor:colorTheme.tabBarBackgroundColor,
                        borderColor:colorTheme.buttonsColor
                    }}>
                        <View style={{flexDirection:'row',alignItems:'center',marginHorizontal:15}}>
                            <TextInput
                                style={{flex:1,color:'white'}}
                                placeholder="Ваше сообщение"
                                placeholderTextColor="#C5C5C5"
                                underlineColorAndroid={'transparent'}
                                onChangeText={(_message) => this.setState({_message})}
                                value={this.state._message}
                            />
                            <TouchableNativeFeedback onPress={()=>this.newMessage()}>
                                <Icon
                                    style={{color: '#C5C5C5',paddingVertical:5,paddingHorizontal:5}}
                                    name={'md-send'} size={20}
                                />
                            </TouchableNativeFeedback>
                        </View>
                    </View>
                </View>
            </View>

        );
    }

}

export default FirstStart;
