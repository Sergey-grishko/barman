import React, {Component} from 'react';
import {
    View, Text, TouchableNativeFeedback,
    TextInput, FlatList, Animated,
    Dimensions, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Image,
    PanResponder, Switch, ToastAndroid
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {CachedImage} from 'react-native-cached-image';
import {Actions} from 'react-native-router-flux';
import {styles, colorTheme, shapesArray} from '../config/config'

const goldButton = require('../../images/goldButton.png')

import Curtain from '../Curtain/_Curtain'
import CurtainView from '../Curtain/_CurtainView'
import _ from "lodash";
import * as Favorite from "../../action/favorite";
import * as Offers from "../../action/offers";
import {BallIndicator} from 'react-native-indicators';

class ProfileBarPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shapesMenuOpen: false,
            curtainOpen: false,
            loading: false,
            favorites: this.props.bar.InFavorites,
            offers: [],
            _search: ''
        }
    }

    async componentDidMount() {
        this.setState({
            loading: true
        })
        let res = await Offers.GetOffersAgent(this.props.bar.Id, 1)
        this.setState({
            offers: res.Records,
            loading: false
        })
    }

    hidePartMenu() {
        this.setState({shapesMenuOpen: false}, () => {
            this.shapes.setValue(1);
            Animated.timing(this.shapes, {toValue: 0, friction: 4}).start();
        });
    }

    openPartMenu() {
        if (this.state.shapesMenuOpen) return this.hidePartMenu()
        this.setState({shapesMenuOpen: true}, () => {
            this.shapes.setValue(0);
            Animated.timing(this.shapes, {toValue: 1}).start();
        });
    }

    addFavorite = async () => {
        this.setState({favorites: !this.state.favorites})
        try {
            let res = await Favorite.AddFavorite(this.props.bar.Id, !this.state.favorites)
            if (res.status !== 204) throw new Error()
            this.props.addFavoriteOther()
        } catch (e) {
            ToastAndroid.show('Ошибка попробуйте еще раз!', ToastAndroid.SHORT);
            this.setState({favorites: this.props.bar.InFavorites})
        }
    }


    renderShapesFlatList(array) {
        return (
            <FlatList
                data={array}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => {
                    return (
                        <View style={{
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: colorTheme.backgroundColor
                        }}>
                            <FontAwesome style={{
                                color: colorTheme.buttonsColor,
                                marginRight: 20
                            }} name={'tag'} size={30}/>
                            <View>
                                <Text style={{color: 'white', fontSize: 16}}>{item.Name}</Text>
                                <Text style={{color: 'white', fontSize: 12, width:250}}>{item.Description}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        )
    }

    render() {

        this.shapes = new Animated.Value(0);
        let shapesView = this.state.offers.length !== 0 ? this.state.offers.slice(0, 2) : null;
        return (
            <ScrollView style={{backgroundColor: colorTheme.backgroundColor}}>
                <CachedImage
                    style={{width: "100%", height: 250}}
                    source={{uri: `http://192.168.10.98/BarmanJack/api/UploadedFiles/` + this.props.bar.LogoImageFileId + `/download`}}
                />
                <View style={{
                    paddingVertical: 10, paddingHorizontal: 20,
                    backgroundColor: colorTheme.tabBarBackgroundColor,
                    alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'
                }}>
                    <View style={{flex: 1}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{
                                fontSize: 18,
                                color: 'white',
                                fontWeight: 'bold'
                            }}>{this.props.bar.Name}</Text>
                            <Text style={{
                                marginLeft: 10,
                                fontSize: 14,
                                color: 'white'
                            }}>{this.props.bar.lengthPosition}0 M</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon style={{color: '#fff'}} name={'ios-pin-outline'} size={25}/>
                            <Text
                                numberOfLines={1}
                                style={{marginLeft: 10, color: 'white', flex: 1}}
                            >
                                {this.props.bar.Address}
                            </Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', width: 70}}>
                        <Image style={{width: 35, height: 35}} source={goldButton}/>
                        <TouchableOpacity onPress={() => this.addFavorite()}>
                            <Icon
                                style={{color: colorTheme.buttonsColor, marginLeft: 10}}
                                name={this.state.favorites ? 'md-heart' : 'ios-heart-outline'}
                                size={35}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{padding: 20, flexDirection: 'row', backgroundColor: colorTheme.backgroundColor}}>
                    <TouchableOpacity>
                        <View style={{
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor: colorTheme.buttonsColor,
                            marginRight: 5
                        }}>

                            <View style={styles.outlineButton}>
                                <Text style={{color: '#fff', fontSize: 12}}>ПРЕДЛОЖЕНИЕ</Text>
                                <FontAwesome style={{color: '#fff', marginLeft: 10}} name={'tag'} size={25}/>
                            </View>

                        </View>
                    </TouchableOpacity>
                    <View style={{
                        backgroundColor: colorTheme.buttonsColor, flexDirection: 'row',
                        paddingVertical: 5, paddingHorizontal: 10, justifyContent: 'space-between',
                        alignItems: 'center', borderRadius: 25, marginRight: 5
                    }}>
                        <Text style={{color: '#fff', fontSize: 12}}>МЕНЮ</Text>
                        <Icon style={{color: '#fff', marginLeft: 10}} name={'md-pizza'} size={25}/>
                    </View>
                    <View
                        style={[styles.centerBlock, styles.circleButton, {height: 40, width: 40, marginRight: 5}]}>
                        <MaterialCommunityIcons style={{color: '#fff'}} name={'information-variant'} size={25}/>
                    </View>
                    <View
                        style={[styles.centerBlock, styles.circleButton, {height: 40, width: 40, marginRight: 5}]}>
                        <FontAwesome style={{color: '#fff'}} name={'briefcase'} size={25}/>
                    </View>
                </View>
                <TouchableNativeFeedback onPress={() => this.openPartMenu()}>
                    <View style={{backgroundColor: colorTheme.backgroundColor}}>
                        {this.renderShapesFlatList(shapesView)}
                        <Animated.View
                            style={{
                                marginTop: this.shapes.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: this.state.offers.length !== 0 ? [-this.state.offers.slice(2).length * 58, 0] : [1, 0]
                                }),
                                zIndex: -1,
                            }}
                        >
                            {this.renderShapesFlatList(this.state.offers.length !== 0 ? this.state.offers.slice(2) : null)}
                        </Animated.View>
                        {
                            this.state.offers.length > 2 ?
                                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                    <Text
                                        style={{color: 'white'}}>{this.state.shapesMenuOpen ? 'Свернуть' : 'Развернуть'}</Text>
                                    <Icon
                                        style={{color: '#fff', marginLeft: 10}}
                                        name={this.state.shapesMenuOpen ? 'ios-arrow-up' : 'ios-arrow-down'}
                                        size={25}
                                    />
                                </View> : null
                        }
                        {this.state.offers.length === 0 ? <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                            <Text style={this.state.loading !== true ? {color: "#fff"} : {display: "none"}}>У этого
                                заведения нет акций</Text>
                        </View>:null}
                        <View><BallIndicator style={this.state.loading === true ? {height:60} : {display: "none"}}
                                             color="#a000ad"/></View>
                    </View>
                </TouchableNativeFeedback>
                <View style={{
                    padding: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: colorTheme.backgroundColor
                }}>
                    <TouchableNativeFeedback onPress={() => Actions.BarMap()}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon style={{color: 'white', marginRight: 10}} name={'ios-pin-outline'} size={25}/>
                            <Text style={{textDecorationLine: 'underline', color: 'white'}}>Показать на карте</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <View style={{borderRadius: 25, borderWidth: 1, borderColor: colorTheme.buttonsColor}}>
                        <TouchableNativeFeedback
                            onPress={() => Actions.JackChat({bar: this.props.bar})}
                            background={TouchableNativeFeedback.Ripple('#52005f', true)}
                            useForeground={false}>
                            <View style={styles.outlineButton}>
                                <Text style={{color: '#fff', fontSize: 12}}>ЧАТ С ДЖЕКОМ</Text>
                                <Icon style={{color: '#fff', marginLeft: 10}} name={'ios-play-outline'} size={25}/>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export default ProfileBarPage;
