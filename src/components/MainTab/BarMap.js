import React, { Component } from 'react';
import {View, Text, TextInput,Image,TouchableNativeFeedback} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import Icon from 'react-native-vector-icons/Ionicons';
import {styles} from '../config/config'
import { Actions } from 'react-native-router-flux';

class FirstStart extends Component{
    constructor(){
        super();

    }
    render() {
        return (
            <View style={{flex:1}}>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    style={{flex:1}}
                    region={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}
                >

                </MapView>
                <View style={{zIndex:1000,position:"absolute",top:10,left:10}}>
                    <TouchableNativeFeedback onPress={()=>Actions.pop()}>
                        <View style={[styles.centerBlock,styles.circleButton]}>
                            <Icon style={{color: '#fff'}} name={'md-arrow-back'} size={25}/>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>

        );
    }

}

export default FirstStart;
