import React, {Component} from 'react';
import {
    View, Text, TouchableNativeFeedback,
    TextInput, FlatList, Animated,
    Dimensions, TouchableWithoutFeedback, ScrollView,
    PanResponder, InteractionManager, Image, AsyncStorage, ToastAndroid, TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {CachedImage} from 'react-native-cached-image';
import {Actions} from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import _ from 'lodash';

import {styles, colorTheme} from '../config/config'

import Curtain from '../Curtain/_Curtain'
import CurtainView from '../Curtain/_CurtainView'
import TopBarView from '../Curtain/_TopBarView'
import * as Bar from '../../action/agents'
import * as Favorite from '../../action/favorite'
import {BallIndicator} from 'react-native-indicators';

class OneBar extends Component {
    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps.item, this.props.item)
    }

    render() {
        const {item, addFavorite, addFavoriteOther} = this.props;
        return (
            <TouchableWithoutFeedback
                delayLongPress={4000}
                onLongPress={() => addFavorite()}
                onPress={() => {
                    Actions.ProfileBarPage({bar: item, addFavoriteOther: addFavoriteOther})
                }}
            >
                <View style={{flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 20}}>
                    <CachedImage style={{width: 125, height: 125, borderRadius: 10}}
                                 source={{uri: `http://192.168.10.98/BarmanJack/api/UploadedFiles/` + item.LogoImageFileId + `/download`}}/>
                    <View style={{marginLeft: 20, flex: 1}}>
                        <Text style={[styles.barListTitle]}>{item.Name}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Icon style={{color: '#fff'}} name={'ios-pin-outline'} size={15}/>
                            <Text numberOfLines={1} style={[styles.barListLocation, {flex: 1}]}>{item.Address}</Text>
                        </View>
                        <Text style={styles.barListLengthPosition}>{item.lengthPosition}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <View style={[styles.centerBlock, {marginRight: 10}]}>
                                <Icon style={{color: colorTheme.buttonsColor}} name={'md-pricetag'} size={35}/>
                                <Text style={{position: 'absolute', color: 'white'}}>{item.OffersCnt}</Text>
                            </View>
                            <TouchableOpacity onPress={() => addFavorite()}>
                                <Icon
                                      style={{color: colorTheme.buttonsColor, paddingHorizontal: 10}}
                                      name={item.InFavorites ? 'md-heart' : 'ios-heart-outline'} size={35}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

class MainPage extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarOnPress: ({previousScene, scene, jumpToIndex}) => {
                const {route, index, focused} = scene;
                if (focused) {
                    if (navigation.state.params.menuStatus()) navigation.state.params.hideMenu()
                    else navigation.state.params.scrollToTop()
                }
                jumpToIndex(0)
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            bar: null,
            curtainOpen: false,
            refreshing: false,
            page: 2,
            name: ''
        }
        this.onUploadData = this.onUploadData.bind(this)
    }


    async componentDidMount() {
        await this.onUploadData()
        this.props.navigation.setParams({
            scrollToTop: this.callScrollToTop.bind(this),
            hideMenu: this.hideMenu.bind(this),
            menuStatus: this.menuStatus.bind(this)
        })
    }

    async onUploadData() {
        this.setState({refreshing: true});
        let res = await Bar.getAll(1, this.state.name)
        this.setState({bar: res.Records, refreshing: false})
    }

    async onEndList() {
        let res = await Bar.getAll(this.state.page, this.state.name)
        let arrNew = this.state.bar.concat(res.Records)
        this.setState({bar: arrNew, page: this.state.page + 1});
    }

    menuStatus() {
        return this.state.curtainOpen
    }

    hideMenu = () => {
        this.setState({curtainOpen: false})
    }

    callScrollToTop = () => {
        this.flatList.scrollToOffset({offset: 0, animated: true});
    };

    addFavoriteOther = (i) => {
        let bar = _.cloneDeep(this.state.bar);
        bar[i].InFavorites = !bar[i].InFavorites;
        this.setState({bar})
    }

    addFavorite = async (i) => {
        let bar = _.cloneDeep(this.state.bar);
        bar[i].InFavorites = !bar[i].InFavorites;
        this.setState({bar})
        try {
            let res = await Favorite.AddFavorite(bar[i].Id, bar[i].InFavorites)
            if (res.status !== 204) throw new Error()
        } catch (e) {
            ToastAndroid.show('Ошибка попробуйте еще раз!', ToastAndroid.SHORT);
        }
    }

    render() {
        return (
            <Curtain
                visible={this.state.curtainOpen}
                onSwipeCurtain={() => this.setState({curtainOpen: false})}
                curtainViewComponent={<CurtainView curtainClose={() => this.setState({curtainOpen: false})}/>}
                topBarViewComponent={
                    <TopBarView
                        curtainOpen={this.state.curtainOpen}
                        onChange={() => this.setState({curtainOpen: !this.state.curtainOpen})}
                    />
                }
            >
                <View style={styles.mainBlock}>
                    <ActionButton
                        style={{zIndex: 50}}
                        position="left"
                        buttonColor={colorTheme.buttonsColor}
                        onPress={()=>Actions.QRCode()}
                        renderIcon={() => <MaterialCommunityIcons name="qrcode" size={35} style={{color: 'white'}}/>}
                    />
                    {this.state.bar !== null ? (
                        <FlatList
                            ref={(ref) => {
                                this.flatList = ref;
                            }}
                            style={{zIndex: 10}}
                            data={this.state.bar}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => <OneBar item={item}
                                                                   addFavorite={() => this.addFavorite(index)}
                                                                   addFavoriteOther={() => this.addFavoriteOther(index)}/>}
                            initialNumToRender={4}
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onUploadData()}
                            onEndReached={() => this.onEndList()}
                            onEndReachedThreshold={3}
                        />
                    ) : <BallIndicator color="#a000ad"/>}
                </View>
            </Curtain>
        );
    }
}

export default MainPage;
