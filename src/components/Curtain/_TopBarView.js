import React, {Component} from 'react';
import {
    View, Text, TouchableNativeFeedback,
    TextInput, FlatList, Animated,
    Dimensions, TouchableWithoutFeedback, ScrollView,
    PanResponder, InteractionManager
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {styles, colorTheme} from '../config/config'
import {Actions} from "react-native-router-flux";

export default class TopBarView extends Component{
    constructor(props) {
        super(props);
        this.state = {
            _search: ''
        }
    }
    render(){
        return(
            <View style={styles.topMenu}>
                <TouchableNativeFeedback onPress={()=>Actions.Profile()}>
                    <View style={[styles.centerBlock,styles.circleButton]}>
                        <Icon style={{color: '#fff'}} name={'md-person'} size={25}/>
                    </View>
                </TouchableNativeFeedback>
                <View style={{flex:1,justifyContent:'center'}}>
                    <TextInput
                        style={{flex:1, color:'white'}} placeholder="Поиск"
                        placeholderTextColor="#fff" underlineColorAndroid="#fff"
                        onChangeText={(_search) => this.setState({_search})}
                        value={this.state._search}
                    />
                    <Icon style={{color: '#fff',position:'absolute',right:10}} name={'md-search'} size={15}/>
                </View>
                <TouchableNativeFeedback onPress={()=>this.props.onChange()}>
                    <View style={[styles.centerBlock,styles.circleButton,this.props.curtainOpen?{backgroundColor:null}:{}]}>
                        <Icon style={{color: this.props.curtainOpen?colorTheme.buttonsColor:'#fff'}} name={'ios-funnel-outline'} size={25}/>
                    </View>
                </TouchableNativeFeedback>
            </View>
        )
    }
}