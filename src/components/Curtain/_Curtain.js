import React, { Component } from 'react';
import {View, Animated, Dimensions, PanResponder,Text} from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';
const {height, width} = Dimensions.get('window');

export default class Curtain extends Component{
    static propTypes = {
        topBarViewComponent:PropTypes.element.isRequired,
        curtainViewComponent:PropTypes.element.isRequired,
        visible:PropTypes.bool
    };

    static defaultProps = {
        visible:false
    };

    state={
        y_translate:new Animated.Value(0),
        pan: new Animated.ValueXY(),
        fadeAnim:new Animated.Value(0),
        menu_expanded:false
    }


    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps, this.props)
    }

    componentDidMount(){
        if(this.state.visible){
            this.openMenu()
        }
        Animated.timing( this.state.fadeAnim,{toValue: 1,useNativeDriver: true}).start();
    }

    componentWillReceiveProps(nextProps) {
        const { visible } = this.props;
        if ((nextProps.visible) && (!visible)) {
            this.setState({visible: true})
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.visible && !prevProps.visible) {
            this.openMenu();
        } else if (!this.props.visible && prevProps.visible) {
            this.hideMenu();
        }
    }
    componentWillMount() {
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponder: (evt, gestureState) =>gestureState.dy<-5,
            onPanResponderGrant: (e, gestureState) => {this.state.pan.setValue({y: 0})},
            onPanResponderMove:(evt, gestureState) => {
                if(gestureState.dy<-5){
                    Animated.event([null, {dy: this.state.pan.y,}])(evt, gestureState);
                }
            },
            onPanResponderRelease: (e, {vy,dy}) => {
                if(Math.abs(vy) >= 0.5|| Math.abs(dy) >= 0.3 * height){
                    if(dy <= 0) this.props.onSwipeCurtain();
                    else {Animated.spring(this.state.pan, {
                        toValue: 0,
                        bounciness: 10
                    }).start();}
                }else{Animated.spring(this.state.pan, {
                    toValue: 0,
                    bounciness: 10
                }).start();}
            }
        });
    }

    hideMenu=()=> {
        this.setState({menu_expanded: false}, () => {
            this.state.y_translate.setValue(1);
            Animated.timing(this.state.fadeAnim, {toValue: 1,useNativeDriver: true}).start();
            Animated.timing(this.state.y_translate, {toValue: 0, friction: 4,useNativeDriver: true}).start();
        });

    }


    openMenu=()=> {
        this.setState({menu_expanded: true}, () => {
            this.state.y_translate.setValue(0);
            Animated.timing(this.state.fadeAnim, {toValue: 0.5,useNativeDriver: true}).start();
            Animated.timing(this.state.y_translate, {toValue: 1,useNativeDriver: true}).start();
            Animated.spring(this.state.pan, {toValue: 0, bounciness: 10}).start();
        });
    }

    render() {
        const {topBarViewComponent,curtainViewComponent,children} = this.props;
        const filter_moveY = this.state.y_translate.interpolate({inputRange: [0, 1], outputRange: [0, height+60]});
        const { pan } = this.state;
        const [translateY] = [pan.y];
        return (
            <View style={{flex:1,backgroundColor:'black'}}>
                <View style={{zIndex:101}}>
                    {topBarViewComponent}
                </View>
                <Animated.View style={{
                    position:'absolute',
                    zIndex:100,
                    width:width,
                    height:height,
                    top:-height,
                    transform: [{translateY: filter_moveY}]
                }}>
                    <Animated.View
                        style={{
                            width:width,
                            zIndex:100,
                            height:height,
                            position:'absolute',
                            transform: [{translateY}]
                        }}
                        {...this._panResponder.panHandlers}
                    >
                        {curtainViewComponent}
                    </Animated.View>
                </Animated.View>
                <Animated.View style={{flex:1,opacity:this.state.fadeAnim}}>
                    {children}
                </Animated.View>
            </View>
        );
    }
}
