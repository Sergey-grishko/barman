import React, {Component} from 'react';
import {
    View, Text, TouchableNativeFeedback,
    TextInput, FlatList, Animated,
    Dimensions, TouchableWithoutFeedback, ScrollView,
    PanResponder, InteractionManager
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles, colorTheme} from '../config/config'

export default class CurtainView extends Component{
    constructor(props) {
        super(props);
        this.state = {
            _category: '',
            _hightway: '',
            _withSuggestions: ''
        }
    }
    render(){
        return(
            <View style={{backgroundColor:colorTheme.tabBarBackgroundColor,paddingHorizontal:20,paddingBottom:20}}>
                <View style={{justifyContent:'center'}}>
                    <Text style={styles.filterTitle}>Фильтр</Text>
                    <TextInput
                        style={{color:'white'}}
                        placeholder="Категория"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(_category) => this.setState({_category})}
                        value={this.state._category}
                    />
                    <Icon style={styles.filterSearchIcon} name={'md-search'} size={15}/>
                </View>
                <View style={{justifyContent:'center'}}>
                    <TextInput
                        style={{color:'white'}}
                        placeholder="Растояние до 2 км."
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(_hightway) => this.setState({_hightway})}
                        value={this.state._hightway}
                    />
                    <Icon style={styles.filterSearchIcon} name={'md-search'} size={15}/>
                </View>
                <View style={{justifyContent:'center'}}>
                    <TextInput
                        style={{color:'white'}}
                        placeholder="С предложениями"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(_withSuggestions) => this.setState({_withSuggestions})}
                        value={this.state._withSuggestions}
                    />
                    <Icon style={styles.filterSearchIcon} name={'md-search'} size={15}/>
                </View>
                <View style={styles.filterEnd}>
                    <Text onPress={()=>this.props.curtainClose()} style={styles.hideFilterButton}>Скрыть фильтр</Text>
                    <TouchableNativeFeedback>
                        <View style={[styles.centerBlock,styles.circleButton]}>
                            <Icon style={{color: '#fff'}} name={'ios-funnel-outline'} size={25}/>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </View>
        )
    }
}