import React, {Component} from 'react';
import {View, Text, TextInput, TouchableNativeFeedback, Alert, TouchableOpacity} from 'react-native';
import {styles, colorTheme} from './config/config'
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from 'react-native-router-flux';
import * as Account from '../action/account'

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

class Registration extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            _rePassword: '',
            error:''
        }
        this.onRegister = this.onRegister.bind(this)
    }

    async onRegister() {
        try {
            if (reg.test(this.state.email) === false) throw new Error("E-mail введен неверно");
            if (this.state.email === '') throw new Error("Поле E-mail пустое");
            if (this.state.password === '') throw new Error("Поле Пароль пустое");
            if (this.state._rePassword === '') throw new Error("Поле Повторите пароль пустое");
            if (this.state._rePassword !== this.state.password) throw new Error("Пароли разные");
            let body = {
                Email: this.state.email,
                Password: this.state.password,
                ConfirmPassword: this.state._rePassword
            }
            let response = await Account.signup(body);
            Actions.Authorization()
        } catch (error) {
            this.setState({
                error:error.message
            })
        }
    }

    render() {
        return (
            <View style={styles.mainBlock}>
                <View style={{flex: 1, justifyContent: 'center', paddingHorizontal: 30}}>
                    <TextInput
                        ref='email'
                        onSubmitEditing={()=>this.refs.password.focus()}
                        returnKeyType="next"
                        keyboardType="email-address"
                        style={{color: 'white', width: '100%'}}
                        placeholder="E-mail"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                    />
                    <TextInput
                        ref="password"
                        onSubmitEditing={()=>this.refs.RePassword.focus()}
                        returnKeyType="next"
                        secureTextEntry={true}
                        style={{color: 'white', width: '100%'}}
                        placeholder="Пароль"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                    />
                    <TextInput
                        ref="RePassword"
                        onSubmitEditing={() => this.onRegister()}
                        returnKeyType="send"
                        secureTextEntry={true}
                        style={{color: 'white', width: '100%'}}
                        placeholder="Повторите пароль"
                        placeholderTextColor="#fff"
                        underlineColorAndroid={colorTheme.buttonsColor}
                        onChangeText={(_rePassword) => this.setState({_rePassword})}
                        value={this.state._rePassword}
                    />
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20}}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={[styles.centerBlock, styles.circleButton, {
                                backgroundColor: '#00629d',
                                borderWidth: 0,
                                marginRight: 5
                            }]}>
                                <Icon style={{color: '#fff'}} name={'logo-facebook'} size={25}/>
                            </View>
                            <View style={[styles.centerBlock, styles.circleButton, {
                                backgroundColor: '#f34733',
                                borderWidth: 0,
                                marginRight: 5
                            }]}>
                                <Icon style={{color: '#fff'}} name={'logo-googleplus'} size={25}/>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.onRegister()}>
                            <View style={styles.outlineButton}>
                                <Text style={{color: '#fff', fontSize: 12}}>РЕГИСТРАЦИЯ</Text>
                                <Icon style={{color: '#fff', marginLeft: 10}} name={'ios-play-outline'} size={25}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{
                        color: 'red',
                        fontSize: 16,
                        textAlign: "center",
                        marginTop: 10
                    }}>{this.state.error}</Text>
                </View>
                <View style={{justifyContent: 'flex-end', alignItems: 'center', marginBottom: 30}}>
                    <TouchableNativeFeedback onPress={() => Actions.pop()}>
                        <Text style={{textDecorationLine: 'underline', color: 'white'}}>Авторизация</Text>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );
    }

}

export default Registration;
