import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableNativeFeedback,
    TextInput,
    FlatList,
    Animated,
    Dimensions,
    TouchableWithoutFeedback,
    ScrollView,
    PanResponder,
    AsyncStorage,
    TouchableOpacity,
    Image,
    ToastAndroid
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from "react-native-router-flux";
import QRCodeScanner from 'react-native-qrcode-scanner';


import {styles, colorTheme} from './config/config'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ActionButton from "react-native-action-button";

class Marker extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'transparent'
            }}>
                <Image source={require("../images/4-layers.png")} style={{
                    height: 180,
                    width: 180,
                }}/>
            </View>
        )
    }
}


class Code extends Component {
    constructor(props) {
        super(props);
        this.state = {
            camera: false
        }
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: colorTheme.backgroundColor,
                alignItems: "center",
                justifyContent: "space-between"
            }}>
                <View style={{
                    alignItems: "center",
                    justifyContent: "center"
                }}>
                    <Text style={{
                        color: '#ffffff',
                        fontSize: 30,
                        marginTop: "20%"
                    }}>Сканируйте QR код</Text>
                    <View style={{flex: 1}}>
                        <QRCodeScanner
                            containerStyle={{
                                alignItems: "center",
                                justifyContent: "center"
                            }}
                            topViewStyle={this.state.camera !== false ? {display: "none"} : null}
                            cameraStyle={this.state.camera === false ? {display: "none"} : {width: 300, height: 300,}}
                            bottomViewStyle={{margin: 0, padding: 0}}
                            showMarker={true}
                            customMarker={<Marker/>}
                            onRead={(e) => console.log(123,e)}
                            topContent={
                                <Image source={require("../images/QRCode.png")}
                                       style={{width: 300, height: 300, marginTop: 120}}/>
                            }
                        />
                    </View>
                    <ActionButton
                        useNativeFeedback={false}
                        style={{marginBottom: 10}}
                        position="center"
                        onPress={() => this.setState({camera: true})}
                        buttonColor={colorTheme.buttonsColor}
                        renderIcon={() => <Icon style={{color: "#fff"}} name="md-qr-scanner" size={35}/>}
                    />
                    <View style={{alignItems: 'center', marginBottom: 20}}>
                        <Text style={{color: "#fff", fontSize: 12}}>СКАНИРОВАТЬ</Text>
                    </View>
                </View>
            </View>
        );
    }

}

export default Code;
