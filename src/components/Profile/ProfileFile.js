import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableNativeFeedback,
    TextInput,
    FlatList,
    Animated,
    Dimensions,
    TouchableWithoutFeedback,
    ScrollView,
    PanResponder,
    AsyncStorage,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from "react-native-router-flux";


import {colorTheme} from '../config/config'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    onExited = () => {
        AsyncStorage.removeItem('access_token')
        Actions.Authorization()
    }


    render() {
        return (
            <View style={{
                backgroundColor: colorTheme.backgroundColor,
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
            }}>
                <TouchableOpacity onPress={()=>this.onExited()}>
                    <Text style={{
                        color: "#fff",
                        backgroundColor: colorTheme.buttonsColor,
                        padding: 20,
                        borderRadius: 20,
                        fontSize: 24
                    }}>ВЫХОД</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

export default Profile;
